scoreboard = [3, 7]
elf_pos = [0, 1]
n = 702831

while len(scoreboard) < n+10:
    cursum = sum(scoreboard[i] for i in elf_pos)
    if cursum >= 10:
        scoreboard.extend([cursum//10, cursum%10])
    else:
        scoreboard.append(cursum)
    elf_pos = [(i + 1 + scoreboard[i]) % len(scoreboard) for i in elf_pos]

print(''.join(str(i) for i in scoreboard[n:n+10]))
