notes = {}
with open('12_input.txt') as f:
    for i, line in enumerate(f):
        if i == 0:
            cur_gen = line.split(':')[1].strip()
        elif line.strip():
            llcrr, gen = line.split(' => ')
            notes[llcrr] = gen.strip()

N = 50000000000
left_index = 0

cur_sum = 0
for _ in range(1000): # enough, pattern just drifts to the right after
    prev_sum = cur_sum
    # expand left and right so there are at least 5 empty pots at beginning and end
    cur_gen = '.....' + cur_gen + '.....'
    left_index -= 5
    next_gen = '..'
    for i in range(2, len(cur_gen) - 2):
        next_gen += notes.get(cur_gen[i-2:i+3], '.')
    cur_gen = next_gen
    cur_sum = sum(i+left_index for i, c in enumerate(cur_gen) if c == '#')

print(cur_sum + (N-1000)*(cur_sum-prev_sum))
