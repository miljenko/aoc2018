regex = open('20_input.txt').read().strip()

dirs = {
    'N': (0, -1),
    'E': (1, 0),
    'S': (0, 1),
    'W': (-1, 0)
}

positions = []
x, y = 0, 0
distances = {(0, 0): 0}

for c in regex:
    if c in dirs:
        dx, dy = dirs[c]
        if (x+dx, y+dy) not in distances or distances[x, y] + 1 < distances[x+dx, y+dy]:
            distances[x+dx, y+dy] = distances[x, y] + 1
        x, y = x+dx, y+dy
    elif c == '(':
        positions.append((x, y))
    elif c == ')':
        x, y = positions.pop()
    elif c == '|':
        x, y = positions[-1]

print(max(distances.values()))
