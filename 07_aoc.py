import re
from collections import defaultdict

steps = defaultdict(list)
prereqs = defaultdict(list)
with open('07_input.txt') as f:
    for line in f:
        s1, s2 = re.findall(r'\s(\w)\s', line)
        steps[s1].append(s2)
        prereqs[s2].append(s1)

order = ''
available = {k for k in steps if not any(k in v for v in steps.values())}
while available:
    # print(order, available)
    next_step = min(available)
    order += next_step
    available.remove(next_step)
    available.update(v for v in steps[next_step] if all(p in order for p in prereqs[v]))

print(order)
