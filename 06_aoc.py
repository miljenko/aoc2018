def distance(x0, y0, x1, y1):
    return abs(x0-x1) + abs(y0-y1)

def edge_coords():
    for i in range(0, max_x+1): # first and last row
        for j in (0, max_y):
            yield i, j
    for i in (0, max_x): # first and last col
        for j in range(1, max_y):
            yield i, j

def is_finite(coord_id):
    return not any(grid[i, j][0] == coord_id for i, j in edge_coords())

coords = set()
with open('06_input.txt') as f:
    for line in f:
        x, y = line.strip().split(',')
        coords.add((int(x), int(y)))

max_x = max(coords, key=lambda t: t[0])[0]
max_y = max(coords, key=lambda t: t[1])[1]

# grid[i, j] = (closest_id, min_distance)
grid = {(i, j): (-1, 999999) for i in range(max_x+1) for j in range(max_y+1)}

for cid, (x, y) in enumerate(coords):
    # print(cid)
    for i in range(max_x+1):
        for j in range(max_y+1):
            dist = distance(i, j, x, y)
            if dist < grid[i, j][1]:
                grid[i, j] = (cid, dist)
            elif dist == grid[i, j][1]:
                grid[i, j] = (-1, dist)

print(max(sum(cid == closest_coord for closest_coord, _ in grid.values())
          for cid in range(len(coords)) if is_finite(cid)))
