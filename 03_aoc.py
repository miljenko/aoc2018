import re
from collections import defaultdict

grid = defaultdict(int)
with open('03_input.txt') as f:
    for line in f:
        n, x, y, w, h = (int(v) for v in re.findall(r'\d+', line))
        for i in range(x, x+w):
            for j in range(y, y+h):
                grid[i, j] += 1

print(sum(c > 1 for _, c in grid.items()))
