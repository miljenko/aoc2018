import re
from collections import defaultdict

def step_time(step):
    return 60 + ord(step) - ord('A') + 1

workers = {i: {'s': '', 't': 0} for i in range(5)}
steps = defaultdict(list)
prereqs = defaultdict(list)
with open('07_input.txt') as f:
    for line in f:
        s1, s2 = re.findall(r'\s(\w)\s', line)
        steps[s1].append(s2)
        prereqs[s2].append(s1)

order = ''
available = {k for k in steps if not any(k in v for v in steps.values())}
t = 0
while True:
    for w in workers:
        if workers[w]['t'] > 1:
            workers[w]['t'] -= 1
        else:
            done_step = workers[w]['s']
            order += done_step
            available.update(v for v in steps[done_step] if all(p in order for p in prereqs[v]))
            if available:
                next_step = min(available)
                available.remove(next_step)
                workers[w] = {'s': next_step, 't': step_time(next_step)}
            else:
                workers[w] = {'s': '', 't': 0}
    # print(t, ''.join(v['s'] or '.' for v in workers.values()), order)
    if not any(w['s'] for w in workers.values()):
        break
    t += 1

print(t)
