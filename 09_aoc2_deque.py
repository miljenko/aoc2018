import re
from collections import deque

p, m = [int(n) for n in re.findall(r'\d+', open('09_input.txt').read())]
m *= 100
scores = [0] * p
pi = 0
circle = deque([0])

for i in range(1, m+1):
    pi = (pi + 1) % p
    if i % 23 == 0:
        scores[pi] += i
        circle.rotate(-7)
        scores[pi] += circle.popleft()
    else:
        circle.rotate(1)
        circle.append(i)
    circle.rotate(1)

print(max(scores))
