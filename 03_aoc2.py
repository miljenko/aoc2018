import re
from collections import defaultdict, namedtuple

Claim = namedtuple('Claim', ['n', 'x', 'y', 'w', 'h'])
claims = []
grid = defaultdict(int)
with open('03_input.txt') as f:
    for line in f:
        n, x, y, w, h = (int(v) for v in re.findall(r'\d+', line))
        claims.append(Claim(n, x, y, w, h))
        for i in range(x, x+w):
            for j in range(y, y+h):
                grid[i, j] += 1

for c in claims:
    if all(grid[i, j] == 1
           for i in range(c.x, c.x+c.w)
           for j in range(c.y, c.y+c.h)):
        print(c.n)
        break
