scoreboard = [3, 7]
elf_pos = [0, 1]
n = 702831
nl = [int(i) for i in str(n)]

while True:
    cursum = sum(scoreboard[i] for i in elf_pos)
    if cursum >= 10:
        scoreboard.extend([cursum//10, cursum%10])
    else:
        scoreboard.append(cursum)
    elf_pos = [(i + 1 + scoreboard[i]) % len(scoreboard) for i in elf_pos]
    if scoreboard[-6:] == nl:
        print(len(scoreboard)-6)
        break
    elif scoreboard[-7:-1] == nl:
        print(len(scoreboard)-7)
        break
