from collections import Counter

n2, n3 = 0, 0
with open('02_input.txt') as f:
    for line in f:
        c = Counter(line)
        n2 += any(c[v] == 2 for v in c)
        n3 += any(c[v] == 3 for v in c)

print(n2*n3)
