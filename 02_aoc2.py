with open('02_input.txt') as f:
    l = [line.strip() for line in f]
    n = len(l)
    for i in range(n):
        for j in range(i+1, n):
            z = list(zip(l[i], l[j]))
            if sum(p[0] != p[1] for p in z) == 1:
                print(''.join(p[0] for p in z if p[0] == p[1]))
                break
