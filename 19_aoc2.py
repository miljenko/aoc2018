import re

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

registers = [0]*6

def addr(a, b, c): registers[c] = registers[a] + registers[b]
def addi(a, b, c): registers[c] = registers[a] + b
def mulr(a, b, c): registers[c] = registers[a] * registers[b]
def muli(a, b, c): registers[c] = registers[a] * b
def banr(a, b, c): registers[c] = registers[a] & registers[b]
def bani(a, b, c): registers[c] = registers[a] & b
def borr(a, b, c): registers[c] = registers[a] | registers[b]
def bori(a, b, c): registers[c] = registers[a] | b
def setr(a, b, c): registers[c] = registers[a]
def seti(a, b, c): registers[c] = a
def gtir(a, b, c): registers[c] = 1 if a > registers[b] else 0
def gtri(a, b, c): registers[c] = 1 if registers[a] > b else 0
def gtrr(a, b, c): registers[c] = 1 if registers[a] > registers[b] else 0
def eqir(a, b, c): registers[c] = 1 if a == registers[b] else 0
def eqri(a, b, c): registers[c] = 1 if registers[a] == b else 0
def eqrr(a, b, c): registers[c] = 1 if registers[a] == registers[b] else 0

program = []
with open('19_input.txt') as f:
    ip = str_to_int_list(f.readline())[0]
    for line in f:
        program.append((line[:4], str_to_int_list(line)))

# output of state when r0 changes:
# 34 seti([0, 7, 0]) [1, 0, 10551282, 34, 10550400, 0] -> [0, 0, 10551282, 34, 10550400, 0]
#  7 addr([1, 0, 0]) [0, 1, 10551282, 7, 1, 10551282] -> [1, 1, 10551282, 7, 1, 10551282]
#  7 addr([1, 0, 0]) [1, 2, 10551282, 7, 1, 5275641] -> [3, 2, 10551282, 7, 1, 5275641]
#  7 addr([1, 0, 0]) [3, 3, 10551282, 7, 1, 3517094] -> [6, 3, 10551282, 7, 1, 3517094]
#  7 addr([1, 0, 0]) [6, 6, 10551282, 7, 1, 1758547] -> [12, 6, 10551282, 7, 1, 1758547]
#  7 addr([1, 0, 0]) [12, 7, 10551282, 7, 1, 1507326] -> [19, 7, 10551282, 7, 1, 1507326]
#  7 addr([1, 0, 0]) [19, 14, 10551282, 7, 1, 753663] -> [33, 14, 10551282, 7, 1, 753663]
#  7 addr([1, 0, 0]) [33, 21, 10551282, 7, 1, 502442] -> [54, 21, 10551282, 7, 1, 502442]
#  7 addr([1, 0, 0]) [54, 42, 10551282, 7, 1, 251221] -> [96, 42, 10551282, 7, 1, 251221]
# ...
# r0 is summing the divisors of r2 (current divisor is in r1)

def divisors(n):
    return [i for i in range(1, n+1) if n % i == 0]

registers[0] = 1
while True:
    i = registers[ip]
    if i < 0 or i >= len(program):
        break
    f, args = program[i]
    before = registers[:]
    locals()[f](*args)
    if before[0] != registers[0]:
        print(f'{i:2} {f}({args}) {before} -> {registers}')
        print(sum(divisors(registers[2])))
        break
    registers[ip] += 1
