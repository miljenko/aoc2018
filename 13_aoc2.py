import sys

moves = { # direction: (dx, dy)
    '<': (-1, 0),
    '>': (1, 0),
    '^': (0, -1),
    'v': (0, 1)
}

intersections = { # (direction, turn): new_direction
    ('<', 'L'): 'v',
    ('>', 'L'): '^',
    ('^', 'L'): '<',
    ('v', 'L'): '>',
    ('<', 'R'): '^',
    ('>', 'R'): 'v',
    ('^', 'R'): '>',
    ('v', 'R'): '<',
    ('<', 'S'): '<',
    ('>', 'S'): '>',
    ('^', 'S'): '^',
    ('v', 'S'): 'v'
}

curves = { # (direction, curve): new_direction
    ('<', '\\'): '^',
    ('>', '\\'): 'v',
    ('^', '\\'): '<',
    ('v', '\\'): '>',
    ('<', '/'): 'v',
    ('>', '/'): '^',
    ('^', '/'): '>',
    ('v', '/'): '<'
}

grid = {}
carts = []
with open('13_input.txt') as f:
    for j, line in enumerate(f):
        for i, c in enumerate(line.strip('\n')):
            if c in '<>v^':
                carts.append((i, j, c, 'L')) # x, y, direction, next turn
                grid[i, j] = '-' if c in '<>' else '|' # if no carts on intersections
            else:
                grid[i, j] = c

SIZE_X, SIZE_Y = i, j+1

def plot(grid, carts):
    pg = grid.copy()
    for x, y, d, t in carts:
        pg[x, y] = d
    for j in range(SIZE_Y):
        print(''.join(pg[i, j] for i in range(SIZE_X+1)))
    print()

# plot(grid, carts)
while True:
    carts.sort()
    for i in range(len(carts)):
        if carts[i][0] == -1:
            continue
        x, y, d, t = carts[i]
        if grid[x, y] == '+': # intersection
            if t == 'L':
                nt = 'S'
            elif t == 'S':
                nt = 'R'
            else: nt = 'L'
            nd = intersections[d, t]
            dx, dy = moves[nd]
            nx, ny = x+dx, y+dy
        elif grid[x, y] in '\\/': # curve
            nd = curves[d, grid[x, y]]
            dx, dy = moves[nd]
            nx, ny, nt = x+dx, y+dy, t
        else: # straight
            dx, dy = moves[d]
            nx, ny, nd, nt = x+dx, y+dy, d, t

        carts[i] = nx, ny, nd, nt

        for k, c in enumerate(carts):
            if k != i and c[:2] == carts[i][:2]:
                carts[k] = (-1, -1, '', '')
                carts[i] = (-1, -1, '', '')
                if sum(t[0] > -1 for t in carts) == 1:
                    print(next(f'{t[0]},{t[1]}' for t in carts if t[0] > -1))
                    sys.exit()

    # plot(grid, carts)
