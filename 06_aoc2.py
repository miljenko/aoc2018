def distance(x0, y0, x1, y1):
    return abs(x0-x1) + abs(y0-y1)

coords = set()
with open('06_input.txt') as f:
    for line in f:
        x, y = line.strip().split(',')
        coords.add((int(x), int(y)))

max_x = max(coords, key=lambda t: t[0])[0]
max_y = max(coords, key=lambda t: t[1])[1]

# grid[i, j] = total_distance
grid = {(i, j): 0 for i in range(max_x+1) for j in range(max_y+1)}

for cid, (x, y) in enumerate(coords):
    # print(cid)
    for i in range(max_x+1):
        for j in range(max_y+1):
            grid[i, j] += distance(i, j, x, y)

print(sum(v < 10000 for v in grid.values()))
