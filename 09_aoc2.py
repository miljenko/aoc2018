import re

class Node():
    def __init__(self, value):
        self.value = value
        self.previous = self
        self.next = self

    def add(self, node):
        node.next = self.next
        node.previous = self
        self.next.previous = node
        self.next = node

    def drop(self):
        self.previous.next = self.next
        self.next.previous = self.previous
        return self.value

p, m = [int(n) for n in re.findall(r'\d+', open('09_input.txt').read())]
m *= 100
scores = [0] * p
pi = 0
curnode = Node(0)

for i in range(1, m+1):
    pi = (pi + 1) % p
    if i % 23 == 0:
        scores[pi] += i
        for _ in range(7):
            curnode = curnode.previous
        next_ = curnode.next
        scores[pi] += curnode.drop()
        curnode = next_
    else:
        nn = Node(i)
        curnode.next.add(nn)
        curnode = nn

print(max(scores))
