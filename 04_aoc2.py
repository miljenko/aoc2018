import re
from collections import defaultdict

guards = defaultdict(lambda: [0]*60)

with open('04_input.txt') as f:
    inp = sorted(f)
    for line in inp:
        if '#' in line:
            gid = int(re.search(r'#(\d+)', line).group(1))
        else:
            minute = int(line[15:17])
            if 'falls' in line:
                begin = minute
            elif 'wakes' in line:
                for i in range(begin, minute):
                    guards[gid][i] += 1

max_val = -1
for gid in guards:
    minute, val = max(enumerate(guards[gid]), key=lambda p: p[1])
    if val > max_val:
        max_val = val
        max_gid = gid
        max_minute = minute

print(max_gid*max_minute)
