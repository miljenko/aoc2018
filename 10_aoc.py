import re

class Point():
    def __init__(self, px, py, vx, vy):
        self.px = px
        self.py = py
        self.vx = vx
        self.vy = vy

points = []
with open('10_input.txt') as f:
    for line in f:
        px, py, vx, vy = [int(n) for n in re.findall(r'-?\d+', line)]
        points.append(Point(px, py, vx, vy))

inf = float('Inf')
min_width = inf
minx, maxx, miny, maxy = inf, -inf, inf, -inf
while True:
    pminx, pmaxx, pminy, pmaxy = minx, maxx, miny, maxy
    minx, maxx, miny, maxy = inf, -inf, inf, -inf
    for p in points:
        p.px += p.vx
        p.py += p.vy
        if p.px < minx: minx = p.px
        elif p.px > maxx: maxx = p.px
        if p.py < miny: miny = p.py
        elif p.py > maxy: maxy = p.py

    if maxx - minx >= min_width:
        break

    grid = {}
    for p in points:
        grid[p.px, p.py] = '#'

    min_width = maxx - minx

for j in range(pminy, pmaxy+1):
    print(''.join(grid.get((i, j), ' ') for i in range(pminx, pmaxx+1)))
