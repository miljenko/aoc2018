from functools import lru_cache

@lru_cache(maxsize=None)
def calc(x, y):
    rid = x + 10
    v = rid * y + 9810
    v *= rid
    v = v // 100 % 10
    v -= 5
    return v

def calc_square(x, y):
    return x, y, sum(calc(i, j) for i in range(x, x+3) for j in range(y, y+3))

N = 300
print(max((calc_square(i, j) for i in range(1, N-2) for j in range(1, N-2)), key=lambda x: x[2]))
