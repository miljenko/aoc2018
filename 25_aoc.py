import re

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'-?\d+', s)]

def distance(p1, p2):
    return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1]) + abs(p1[2]-p2[2]) + abs(p1[3]-p2[3])

constellations = []
with open('25_input.txt') as f:
    for line in f:
        coords = tuple(str_to_int_list(line))
        added_to = -1
        for i, c in enumerate(constellations):
            for p in c:
                if distance(p, coords) <= 3:
                    if added_to != -1: # merge
                        constellations[added_to].update(c)
                        c.clear()
                    else:
                        c.add(coords)
                        added_to = i
                    break
        if added_to == -1:
            constellations.append(set([coords]))

print(sum(len(c) > 0 for c in constellations))
