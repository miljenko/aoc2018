import re
from collections import defaultdict

class Sample():
    def __init__(self, instr, before, after):
        self.instr = str_to_int_list(instr)
        self.before = str_to_int_list(before)
        self.after = str_to_int_list(after)

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

registers = [0]*4

def addr(a, b, c): registers[c] = registers[a] + registers[b]
def addi(a, b, c): registers[c] = registers[a] + b
def mulr(a, b, c): registers[c] = registers[a] * registers[b]
def muli(a, b, c): registers[c] = registers[a] * b
def banr(a, b, c): registers[c] = registers[a] & registers[b]
def bani(a, b, c): registers[c] = registers[a] & b
def borr(a, b, c): registers[c] = registers[a] | registers[b]
def bori(a, b, c): registers[c] = registers[a] | b
def setr(a, b, c): registers[c] = registers[a]
def seti(a, b, c): registers[c] = a
def gtir(a, b, c): registers[c] = 1 if a > registers[b] else 0
def gtri(a, b, c): registers[c] = 1 if registers[a] > b else 0
def gtrr(a, b, c): registers[c] = 1 if registers[a] > registers[b] else 0
def eqir(a, b, c): registers[c] = 1 if a == registers[b] else 0
def eqri(a, b, c): registers[c] = 1 if registers[a] == b else 0
def eqrr(a, b, c): registers[c] = 1 if registers[a] == registers[b] else 0

instrs = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti,
          gtir, gtri, gtrr, eqir, eqri, eqrr]

samples = []
program = []
possible = defaultdict(set)
impossible = defaultdict(set)
with open('16_input.txt') as f:
    lines = f.readlines()
    while lines[0].startswith('Before'):
        samples.append(Sample(lines[1], lines[0], lines[2]))
        lines = lines[4:]

    for line in lines:
        if line.strip():
            program.append(str_to_int_list(line))

for s in samples:
    opcode = s.instr[0]
    for ins in instrs:
        registers = s.before.copy()
        ins(s.instr[1], s.instr[2], s.instr[3])
        if registers == s.after and ins not in impossible[opcode]:
            possible[opcode].add(ins)
        if registers != s.after:
            impossible[opcode].add(ins)
            possible[opcode].discard(ins)

l = possible.items()
while not all(len(v) == 1 for k, v in l):
    l = sorted(l, key=lambda i: len(i[1]))
    for i in range(16):
        if len(l[i][1]) == 1:
            for j in range(i+1, 16):
                l[j][1].discard(next(iter(l[i][1])))

opcodes = {o: f.pop() for o, f in l}
registers = [0]*4
for p in program:
    opcodes[p[0]](p[1], p[2], p[3])
print(registers[0])
