import string

inp = open('05_input.txt').read().strip()
pairs = {(c, c.swapcase()) for c in string.ascii_letters}

res = inp[0]
remaining = inp[1:]
while remaining:
    if (res[-1], remaining[0]) in pairs:
        res = res[:-1]
        if not res:
            res = remaining[1]
            remaining = remaining[2:]
        else:
            remaining = remaining[1:]
    else:
        res += remaining[0]
        remaining = remaining[1:]

print(len(res))
