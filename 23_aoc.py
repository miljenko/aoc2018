import re

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'-?\d+', s)]

def distance(b1, b2):
    return abs(b1[0]-b2[0]) + abs(b1[1]-b2[1]) + abs(b1[2]-b2[2])

bots = []
with open('23_input.txt') as f:
    for line in f:
        *pos, r = str_to_int_list(line)
        bots.append((pos, r))

maxr = max(bots, key=lambda t: t[1])
print(sum(distance(bot[0], maxr[0]) <= maxr[1] for bot in bots))
