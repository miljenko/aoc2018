import re
import sys
from collections import defaultdict

sys.setrecursionlimit(10000)
input_re = re.compile(r'([xy])=(\d+), ([xy])=(\d+)\.\.(\d+)')
grid = defaultdict(lambda: 'x')

def flow(x, y, to='D'):
    grid[x, y] = '|'
    down = x, y+1
    if grid[down] == '.':
        flow(*down)
    if grid[down] == '|':
        return False

    l, r = (x-1, y), (x+1, y)
    left_ok = grid[l] == '#' or grid[l] == '.' and flow(*l, 'L')
    right_ok = grid[r] == '#' or grid[r] == '.' and flow(*r, 'R')

    if to == 'D' and left_ok and right_ok:
        grid[x, y] = '~'
        while grid[l] == '|':
            grid[l] = '~'
            l = (l[0]-1, l[1])
        while grid[r] == '|':
            grid[r] = '~'
            r = (r[0]+1, r[1])

    return to == 'L' and left_ok or\
           to == 'R' and right_ok

inf = float('Inf')
min_x, min_y, max_x, max_y = inf, inf, -inf, -inf
with open('17_input.txt') as f:
    for line in f:
        m = re.match(input_re, line)
        r1 = range(int(m.group(2)), int(m.group(2))+1)
        r2 = range(int(m.group(4)), int(m.group(5))+1)
        x_range = r1 if m.group(1) == 'x' else r2
        y_range = r1 if m.group(1) == 'y' else r2
        min_x = min(min_x, x_range.start)
        max_x = max(max_x, x_range.stop-1)
        min_y = min(min_y, y_range.start)
        max_y = max(max_y, y_range.stop-1)
        for i in x_range:
            for j in y_range:
                grid[i, j] = '#'

# add column right and left so water can overflow from left-most and right-most container
min_x -= 1
max_x += 1
for x in range(min_x, max_x+1):
    for y in range(0, max_y+1):
        if (x, y) not in grid:
            grid[x, y] = '.'

flow(500, 0)

with open('17_grid.txt', 'w') as fg:
    for y in range(0, max_y+1):
        fg.write(''.join(grid[x, y] for x in range(min_x, max_x+1)) + '\n')

# part 1
print(sum(grid[i, j] in '|~' for i in range(min_x, max_x+1) for j in range(min_y, max_y+1)))
# part 2
print(sum(grid[i, j] == '~' for i in range(min_x, max_x+1) for j in range(min_y, max_y+1)))
