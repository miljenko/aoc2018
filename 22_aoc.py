import re
from functools import lru_cache

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

with open('22_input.txt') as f:
    DEPTH = str_to_int_list(f.readline())[0]
    TX, TY = str_to_int_list(f.readline())

@lru_cache(maxsize=None)
def geo_index(x, y):
    if x == 0 and y == 0:
        return 0
    if x == TX and y == TY:
        return 0
    if y == 0:
        return x * 16807
    if x == 0:
        return y * 48271
    return erosion(x-1, y) * erosion(x, y-1)

@lru_cache(maxsize=None)
def erosion(x, y):
    return (geo_index(x, y) + DEPTH) % 20183

@lru_cache(maxsize=None)
def risk(x, y):
    return erosion(x, y) % 3

print(sum(risk(i, j) for i in range(TX+1) for j in range(TY+1)))
