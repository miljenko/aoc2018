import re

class Sample():
    def __init__(self, instr, before, after):
        self.instr = str_to_int_list(instr)
        self.before = str_to_int_list(before)
        self.after = str_to_int_list(after)

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

registers = [0]*4

def addr(a, b, c): registers[c] = registers[a] + registers[b]
def addi(a, b, c): registers[c] = registers[a] + b
def mulr(a, b, c): registers[c] = registers[a] * registers[b]
def muli(a, b, c): registers[c] = registers[a] * b
def banr(a, b, c): registers[c] = registers[a] & registers[b]
def bani(a, b, c): registers[c] = registers[a] & b
def borr(a, b, c): registers[c] = registers[a] | registers[b]
def bori(a, b, c): registers[c] = registers[a] | b
def setr(a, b, c): registers[c] = registers[a]
def seti(a, b, c): registers[c] = a
def gtir(a, b, c): registers[c] = 1 if a > registers[b] else 0
def gtri(a, b, c): registers[c] = 1 if registers[a] > b else 0
def gtrr(a, b, c): registers[c] = 1 if registers[a] > registers[b] else 0
def eqir(a, b, c): registers[c] = 1 if a == registers[b] else 0
def eqri(a, b, c): registers[c] = 1 if registers[a] == b else 0
def eqrr(a, b, c): registers[c] = 1 if registers[a] == registers[b] else 0

instrs = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti,
          gtir, gtri, gtrr, eqir, eqri, eqrr]

samples = []
with open('16_input.txt') as f:
    lines = f.readlines()
    while lines[0].startswith('Before'):
        samples.append(Sample(lines[1], lines[0], lines[2]))
        lines = lines[4:]

res = 0
for s in samples:
    n = 0
    for ins in instrs:
        registers = s.before.copy()
        ins(s.instr[1], s.instr[2], s.instr[3])
        if registers == s.after:
            n += 1
    if n >= 3:
        res += 1

print(res)
