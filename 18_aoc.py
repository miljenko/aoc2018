from collections import Counter

grid = {}
N = 50

def adjacent(x, y):
    for dx in range(-1, 2):
        for dy in range(-1, 2):
            nx, ny = x+dx, y+dy
            if 0 <= nx < N and 0 <= ny < N and (dx != 0 or dy != 0):
                yield nx, ny

def calc(x, y):
    cur = grid[x, y]
    if cur == '.':
        return '|' if sum(grid[ax, ay] == '|' for ax, ay in adjacent(x, y)) >= 3 else '.'
    if cur == '|':
        return '#' if sum(grid[ax, ay] == '#' for ax, ay in adjacent(x, y)) >= 3 else '|'
    return '#' if any(grid[ax, ay] == '#' for ax, ay in adjacent(x, y)) and\
                  any(grid[ax, ay] == '|' for ax, ay in adjacent(x, y))\
               else '.'

with open('18_input.txt') as f:
    for j, line in enumerate(f):
        for i, c in enumerate(line.strip()):
            grid[i, j] = c

for _ in range(10):
    new_grid = {}
    for i in range(N):
        for j in range(N):
            new_grid[i, j] = calc(i, j)
    grid = new_grid.copy()

c = Counter(grid.values())
print(c['|'] * c['#'])
