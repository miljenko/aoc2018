from functools import lru_cache

def calc(x, y):
    rid = x + 10
    v = rid * y + 9810
    v *= rid
    v = v // 100 % 10
    v -= 5
    return v

@lru_cache(maxsize=None)
def calc_square(x, y, size):
    if size > 3:
        half = size//2
        if size % 2 == 0:
            return (calc_square(x, y, half)
                    + calc_square(x+half, y, half)
                    + calc_square(x, y+half, half)
                    + calc_square(x+half, y+half, half))
        else: # odd size - 2 squares size h, 2 size h+1
            return (calc_square(x, y, half+1)
                    + calc_square(x+half+1, y, half)
                    + calc_square(x, y+half+1, half)
                    + calc_square(x+half, y+half, half+1)
                    - grid[x+half, y+half]) # counted twice
    return sum(grid[i, j] for i in range(x, x+size) for j in range(y, y+size))

N = 300
grid = {(i, j): calc(i, j) for i in range(1, N+1) for j in range(1, N+1)}

max_x, max_y, max_size, max_val = 0, 0, 0, 0
for s in range(1, N+1):
    for i in range(1, N-s+2):
        for j in range(1, N-s+2):
            p = calc_square(i, j, s)
            if p > max_val:
                max_x, max_y, max_size, max_val = i, j, s, p
                print(f'current max = {i},{j},{s} = {p}')

print(f'{max_x},{max_y},{max_size}')
