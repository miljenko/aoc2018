import re
from collections import deque
from functools import lru_cache

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

with open('22_input.txt') as f:
    DEPTH = str_to_int_list(f.readline())[0]
    TX, TY = str_to_int_list(f.readline())

# DEPTH = 510
# TX, TY = 10, 10

@lru_cache(maxsize=None)
def geo_index(x, y):
    if x == 0 and y == 0:
        return 0
    if x == TX and y == TY:
        return 0
    if y == 0:
        return x * 16807
    if x == 0:
        return y * 48271
    return erosion(x-1, y) * erosion(x, y-1)

@lru_cache(maxsize=None)
def erosion(x, y):
    return (geo_index(x, y) + DEPTH) % 20183

@lru_cache(maxsize=None)
def risk(x, y):
    return erosion(x, y) % 3

def neighbors(x, y):
    adjs = [(x+1, y), (x, y+1), (x-1, y), (x, y-1)]
    for ax, ay in adjs:
        if ax >= 0 and ay >= 0:
            cur_forbidden_tool = risk(x, y)
            next_forbidden_tool = risk(ax, ay)
            # risk/forbidden tool id
            # 0 - rocky - neither
            # 1 - wet - torch
            # 2 - narrow - climbing gear

            for next_tool in range(3):
                if next_tool not in (cur_forbidden_tool, next_forbidden_tool):
                    yield ax, ay, next_tool

start = (0, 0, 1) # x, y, current tool
goal = (TX, TY, 1)
frontier = deque([start])
came_from = {start: start}
cost_so_far = {start: 0}

while frontier:
    current = frontier.popleft()
    cur_x, cur_y, cur_tool = current

    # expand search 100 past target
    if cur_x > TX + 100 or cur_y > TY + 100:
        continue

    for neighbor in neighbors(cur_x, cur_y):
        new_cost = cost_so_far[current] + (1 if cur_tool == neighbor[2] else 8)
        if neighbor not in came_from or cost_so_far[neighbor] > new_cost:
            frontier.append(neighbor)
            came_from[neighbor] = current
            cost_so_far[neighbor] = new_cost
            if neighbor == goal:
                print(cost_so_far[neighbor], len(frontier), len(set(frontier)))

print(cost_so_far[goal])
