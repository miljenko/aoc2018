from itertools import cycle

freqs = set([0])
curr = 0
l = [int(i) for i in open('01_input.txt')]
for i in cycle(l):
    curr += i
    if curr in freqs:
        print(curr)
        break
    freqs.add(curr)
