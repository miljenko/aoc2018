import re

def str_to_int_list(s):
    return [int(n) for n in re.findall(r'\d+', s)]

registers = [0]*6

def addr(a, b, c): registers[c] = registers[a] + registers[b]
def addi(a, b, c): registers[c] = registers[a] + b
def mulr(a, b, c): registers[c] = registers[a] * registers[b]
def muli(a, b, c): registers[c] = registers[a] * b
def banr(a, b, c): registers[c] = registers[a] & registers[b]
def bani(a, b, c): registers[c] = registers[a] & b
def borr(a, b, c): registers[c] = registers[a] | registers[b]
def bori(a, b, c): registers[c] = registers[a] | b
def setr(a, b, c): registers[c] = registers[a]
def seti(a, b, c): registers[c] = a
def gtir(a, b, c): registers[c] = 1 if a > registers[b] else 0
def gtri(a, b, c): registers[c] = 1 if registers[a] > b else 0
def gtrr(a, b, c): registers[c] = 1 if registers[a] > registers[b] else 0
def eqir(a, b, c): registers[c] = 1 if a == registers[b] else 0
def eqri(a, b, c): registers[c] = 1 if registers[a] == b else 0
def eqrr(a, b, c): registers[c] = 1 if registers[a] == registers[b] else 0

program = []
with open('19_input.txt') as f:
    ip = str_to_int_list(f.readline())[0]
    for line in f:
        program.append((line[:4], str_to_int_list(line)))

while True:
    i = registers[ip]
    if i < 0 or i >= len(program):
        break
    f, args = program[i]
    locals()[f](*args)
    registers[ip] += 1

print(registers[0])
