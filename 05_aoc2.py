import re
import string

inp = open('05_input.txt').read().strip()
pairs = {(c, c.swapcase()) for c in string.ascii_letters}

def collapse(polymer):
    res = polymer[0]
    remaining = polymer[1:]
    while remaining:
        if (res[-1], remaining[0]) in pairs:
            res = res[:-1]
            if not res:
                res = remaining[1]
                remaining = remaining[2:]
            else:
                remaining = remaining[1:]
        else:
            res += remaining[0]
            remaining = remaining[1:]

    return len(res)

print(min(collapse(re.sub(c, '', inp, flags=re.I)) for c in string.ascii_lowercase))
