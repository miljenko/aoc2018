import re

tree = {}
next_id = 1
def read_node(parent_id, remaining):
    global next_id
    node_id = next_id
    next_id += 1
    nchild = remaining.pop(0)
    nmeta = remaining.pop(0)
    tree[node_id] = {'c': [], 'm': []}
    if parent_id:
        tree[parent_id]['c'].append(node_id)
    for _ in range(nchild):
        remaining = read_node(node_id, remaining)

    tree[node_id]['m'] = remaining[:nmeta]

    return remaining[nmeta:]

with open('08_input.txt') as f:
    inp = [int(n) for n in re.findall(r'\d+', f.read())]

# inp = [int(n) for n in '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'.split()]
unprocessed = inp
while unprocessed:
    unprocessed = read_node(None, unprocessed)

print(sum(sum(v['m']) for v in tree.values()))
